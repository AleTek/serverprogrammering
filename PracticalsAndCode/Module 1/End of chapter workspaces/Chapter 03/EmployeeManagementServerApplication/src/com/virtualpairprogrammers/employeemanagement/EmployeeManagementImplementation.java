package com.virtualpairprogrammers.employeemanagement;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
public class EmployeeManagementImplementation implements
		EmployeeManagementService {

	public void registerEmployee(Employee newEmployee) {
		
	}

	public List<Employee> getAllEmployees() {
		Employee emp1 = new Employee("James","Green","Writer",1700);
		Employee emp2 = new Employee("Sharon","White","Editor",2100);
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(emp1);
		employees.add(emp2);
		return employees;
	}


	public List<Employee> searchBySurname(String surname) {
		return null;
	}

}
