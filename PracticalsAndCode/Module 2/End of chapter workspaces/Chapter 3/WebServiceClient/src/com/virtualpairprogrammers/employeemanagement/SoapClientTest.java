package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

public class SoapClientTest {

	public static void main(String[] args) {
		EmployeeManagementWebService service = new EmployeeManagementWebserviceImplementationService().getEmployeeManagementWebServicePort(); 
		
		Employee newEmployee = new Employee();
		newEmployee.setFirstName("Jim");
		newEmployee.setSurname("Brown");
		newEmployee.setJobRole("dancer");
		newEmployee.setSalary(1000);
		
		try {
			service.registerNewEmployee(newEmployee);
		} catch (ServiceUnavailableException_Exception e) {
			System.out.println("A third party connection was down, please try again");
		}
			
		
		List<Employee> employees = service.getAllEmployees();
		for (Employee employee : employees) {
			System.out.println("id : " + employee.getId() + " Name : " + employee.getFirstName() + " " + employee.getSurname());
		}
		
		Employee employee102 = service.getEmployeeById(102);
		System.out.println("id : " + employee102.getId() + " Name : " + employee102.getFirstName() + " " + employee102.getSurname());
	}

}
