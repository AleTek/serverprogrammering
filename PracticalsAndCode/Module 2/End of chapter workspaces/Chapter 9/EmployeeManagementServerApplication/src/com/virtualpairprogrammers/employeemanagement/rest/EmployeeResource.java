package com.virtualpairprogrammers.employeemanagement.rest;

import java.net.URI;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.virtualpairprogrammers.employeemanagement.EmployeeManagementServiceLocal;
import com.virtualpairprogrammers.employeemanagement.ServiceUnavailableException;
import com.virtualpairprogrammers.employeemanagement.dataaccess.EmployeeNotFoundException;
import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@Path("/employees")
public class EmployeeResource {

	@Inject
	private EmployeeManagementServiceLocal service;
	
	@GET
	@Produces({"application/JSON"})
	public List<Employee> getAllEmployees() {
		return service.getAllEmployees();
	}
	
	@GET
	@Produces({"application/JSON", "application/XML"})
	@Path("{employeeNo}")
	public Response findEmployeeById(@PathParam("employeeNo") int id, @Context HttpHeaders headers) {
		System.out.println("HEADERS : " + headers.getRequestHeaders());
		try {
			Employee result = service.getById(id);
			return Response.ok(result).build();
		} catch (EmployeeNotFoundException e) {
			// TODO Auto-generated catch block
			return Response.status(404).build();
		}
	}
	
	@POST
	@Produces({"application/JSON", "application/XML"})
	@Consumes({"application/JSON"})
	public Response createEmployee(Employee employee) {
		try {
			service.registerEmployee(employee);
			
			URI uri = null;
			try {
				uri = new URI("/employees/104");
			}
			catch (Exception e) {}
			
			return Response.created(uri).build();
		} catch (ServiceUnavailableException e) {
			return Response.status(504).build();
		}
		
	}
}
