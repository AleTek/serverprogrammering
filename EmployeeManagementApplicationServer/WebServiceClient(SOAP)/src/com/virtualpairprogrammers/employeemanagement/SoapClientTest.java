package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

public class SoapClientTest {

	public static void main(String[] args) {
		EmployeeManagementWebService service = new EmployeeManagementWebServiceImplementationService().getEmployeeManagementWebServicePort();

		Employee newEmployee = new Employee();
		newEmployee.setFirstName("Dom");
		newEmployee.setSurname("Cobb");
		newEmployee.setJobRole("extractor");
		newEmployee.setSalary(4000000);
		
		try {
			service.registerNewEmployee(newEmployee);
		}
		catch(ServiceUnavailableException_Exception e){
			System.err.println("A third part connection was down, please try again");
		}
		
		List<Employee> employees = service.getAllEmployees();
	
		for (Employee employee : employees) {
			System.out.println("id: " + employee.getId() + " "+ "Name: " + employee.getFirstName() + " " + employee.getSurname());
		}
		
		Employee employee102 = service.getEmployeeById(102);
		System.out.println("id: " + employee102.getId() + "Name: " + employee102.getFirstName() + " " + employee102.getSurname());
		
	}

}
