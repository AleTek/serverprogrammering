package se.fotboll.fotbollsregister.dataaccess;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import se.fotboll.fotbollsregister.domain.Spelare;
@Stateless
@Alternative
public class FotbollsregisterDataAccessTestingVersion implements FotbollsregisterDataAccess {

	@Override
	public void registreraSpelare(Spelare spelare) {

	}

	@Override
	public List<Spelare> taFramSpelare() {
		Spelare salah = new Spelare("Muhammed", "Salah",1992, "Liverpool");
		Spelare mane = new Spelare("Sadio", "Mane",1992, "Juventus");
		List<Spelare>fotbollsregister = new ArrayList();
		fotbollsregister.add(salah);
		fotbollsregister.add(mane);
		return fotbollsregister;
	}

}
