package se.fotboll.fotbollsregister.dataaccess;

import java.util.List;

import javax.ejb.Local;

import se.fotboll.fotbollsregister.domain.Spelare;

@Local
public interface FotbollsregisterDataAccess {
public void registreraSpelare(Spelare spelare);
public List<Spelare>taFramSpelare();
}
