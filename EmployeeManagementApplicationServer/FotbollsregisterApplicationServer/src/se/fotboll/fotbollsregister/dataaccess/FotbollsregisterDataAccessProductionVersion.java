package se.fotboll.fotbollsregister.dataaccess;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.fotboll.fotbollsregister.domain.Spelare;

@Stateless
public class FotbollsregisterDataAccessProductionVersion implements FotbollsregisterDataAccess {

	@Override
	public void registreraSpelare(Spelare spelare) {
	}

	@Override
	public List<Spelare> taFramSpelare() {
		Spelare messi = new Spelare("Lionel", "Messi",1986, "Barcelona");
		Spelare cr7 = new Spelare("Cristiano", "Ronaldo",1986, "Juventus");
		List<Spelare>fotbollsregister = new ArrayList();
		fotbollsregister.add(messi);
		fotbollsregister.add(cr7);
		return fotbollsregister;
	}

}
