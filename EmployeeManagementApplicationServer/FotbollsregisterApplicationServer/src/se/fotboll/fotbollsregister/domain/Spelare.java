package se.fotboll.fotbollsregister.domain;

import java.io.Serializable;

public class Spelare implements Serializable {
	private String f�rnamn;
	private String efternamn;
	private int f�delse�r;
	private String lag;
	
	public Spelare(String f�rnamn, String efternamn, int f�delse�r, String lag) {
		this.f�rnamn = f�rnamn;
		this.efternamn = efternamn;
		this.f�delse�r = f�delse�r;
		this.lag = lag;
	}

	@Override
	public String toString() {
		return f�rnamn + " "+efternamn + ", " + f�delse�r + ", " + lag;
	}
	
}
