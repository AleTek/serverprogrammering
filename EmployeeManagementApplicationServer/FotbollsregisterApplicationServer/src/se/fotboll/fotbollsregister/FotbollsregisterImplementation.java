package se.fotboll.fotbollsregister;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

import se.fotboll.fotbollsregister.dataaccess.FotbollsregisterDataAccess;
import se.fotboll.fotbollsregister.domain.Spelare;

@Stateless
@Default
public class FotbollsregisterImplementation implements FotbollsregisterService {

	@Inject
	private FotbollsregisterDataAccess fda;
	
	@Override
	public void registreraSpelare(Spelare spelare) {
		
	}

	@Override
	public List<Spelare> taFramSpelare() {
	return	fda.taFramSpelare();
	}

}
