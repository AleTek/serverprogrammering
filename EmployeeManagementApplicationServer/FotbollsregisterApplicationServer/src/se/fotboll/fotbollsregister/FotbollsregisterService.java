package se.fotboll.fotbollsregister;

import java.util.List;

import javax.ejb.Remote;

import se.fotboll.fotbollsregister.domain.Spelare;

@Remote
public interface FotbollsregisterService {
public void registreraSpelare(Spelare spelare);
public List<Spelare>taFramSpelare();
}
