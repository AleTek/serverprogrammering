package com.virtualpairprogrammers.employeemanagement.dataaccess;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@Alternative
public class EmployeeDataAccessTestingVersion implements EmployeeDataAccess {

	@Override
	public void insert(Employee employee) {
		
	}

	@Override
	public List<Employee> findAll() {
		Employee e1 = new Employee("Kalle", "Anka", "Kvackare", 23000);
		Employee e2 = new Employee("Musse", "Pigg", "Deckare", 25000);
		Employee e3 = new Employee("Mimmi", "Pigg", "Deckare", 25000);
		Employee e4 = new Employee("L�ngben", "blabla", "Deckare", 25000);
		
		List<Employee> employees = new ArrayList<>();
		employees.add(e1);
		employees.add(e2);
		employees.add(e3);
		employees.add(e4);
		return employees;
	}

	@Override
	public List<Employee> findBySurname(String surname) {
		return null;
	}
	@Override
	public Employee findById(int id) {
		return null;
	}
}
