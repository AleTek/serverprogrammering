package com.virtualpairprogrammers.employeemanagement.dataaccess;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@Default
public class EmployeeDataAccessProductionVersion implements EmployeeDataAccess {
	@PersistenceContext
	private EntityManager em;

	@Override
	public void insert(Employee employee) {
		em.persist(employee);
	}

	@Override
	public List<Employee> findAll() {
		Query q = em.createQuery("select employee from Employee employee");
		
		List<Employee> employees = q.getResultList();
		return employees;
	}

	@Override
	public List<Employee> findBySurname(String surname) {
		Query q = em.createQuery("select employee from Employee employee where surname = :surname");
		q.setParameter("surname",surname);
		return q.getResultList();
	}

//	@Override
//	public Employee findById(int id) {
//		Query q = em.createQuery("select e from Employee e where id = :id");
//		q.setParameter("id", id);
//		return (Employee) q.getSingleResult();
//		
//	}
	@Override
	public Employee findById(int id) throws EmployeeNotFoundException {
		Query q = em.createQuery("select e from Employee e where id = :id");
		q.setParameter("id", id);
		try {
		return (Employee) q.getSingleResult();
		}
		catch(NoResultException e) {
			throw new EmployeeNotFoundException();
		}
	}
}
