package com.virtualpairprogrammers.employeemanagement.dataaccess;

import java.util.List;

import javax.ejb.Local;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Local
public interface EmployeeDataAccess {
	public void insert (Employee employee);
	public List<Employee> findAll();
	public List <Employee> findBySurname(String surname);
	public Employee findById(int id) throws EmployeeNotFoundException;
} 
