package com.virtualpairprogrammers.employeemanagement;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.virtualpairprogrammers.employeemanagement.dataaccess.EmployeeDataAccess;
import com.virtualpairprogrammers.employeemanagement.dataaccess.EmployeeNotFoundException;
import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
public class EmployeeManagementImplementation implements EmployeeManagementService, EmployeeManagementServiceLocal {
	
	@Inject
	private EmployeeDataAccess dao;
	
	@Override
	public void registerEmployee(Employee employee) {
		dao.insert(employee);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return dao.findAll();

	}

	@Override
	public List<Employee> searchBySurname(String surname) {
		return dao.findBySurname(surname);
	}
	
	@Override
	public Employee getEmployeeById(int id) throws EmployeeNotFoundException{
		return dao.findById(id);
	}; 

}
