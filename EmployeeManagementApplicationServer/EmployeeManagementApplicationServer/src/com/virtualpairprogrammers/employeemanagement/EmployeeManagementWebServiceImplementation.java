package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import com.virtualpairprogrammers.employeemanagement.dataaccess.EmployeeNotFoundException;
import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@WebService(name="EmployeeManagementWebService")
public class EmployeeManagementWebServiceImplementation {

	public EmployeeManagementWebServiceImplementation() {
	}

	@Inject 
	private EmployeeManagementServiceLocal service;
	
	public List<Employee> getAllEmployees(){
		return service.getAllEmployees();
	}

	public Employee getEmployeeById(int id) throws EmployeeNotFoundException {
		return service.getEmployeeById(id);
	}
//	Test tillagd 25/2
	public void registerNewEmployee(Employee employee) throws ServiceUnavailableException {
		service.registerEmployee(employee);
	}
}
