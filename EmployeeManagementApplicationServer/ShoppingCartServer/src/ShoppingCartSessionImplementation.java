
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

@Stateful
public class ShoppingCartSessionImplementation implements ShoppingCartSession {
	private List <String> items;

	public ShoppingCartSessionImplementation() {
		this.items = new ArrayList<String>();
	}
	
	/* (non-Javadoc)
	 * @see ShoppingCartSession#addItem(java.lang.String)
	 */
	@Override
	public void addItem (String newItem) {
		this.items.add(newItem);
	}
	
	/* (non-Javadoc)
	 * @see ShoppingCartSession#getCartContents()
	 */
	@Override
	public List<String> getCartContents () {
		return this.items;
	} 
	
	/* (non-Javadoc)
	 * @see ShoppingCartSession#checkout()
	 */
	@Override
	@Remove
	public void checkout() {
		
	}
}
