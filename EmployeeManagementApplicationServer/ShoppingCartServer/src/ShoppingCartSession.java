
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface ShoppingCartSession {

	void addItem(String newItem);

	List<String> getCartContents();

	void checkout();

}