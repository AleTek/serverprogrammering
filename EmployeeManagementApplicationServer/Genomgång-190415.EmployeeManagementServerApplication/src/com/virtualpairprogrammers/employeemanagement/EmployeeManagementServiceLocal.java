package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

import javax.ejb.Local;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Local
public interface EmployeeManagementServiceLocal {
	public List<Employee> getAllEmployees();
	public List<Employee> searchBySurname(String surname);
	public Employee searchById(int id);
	
}
