package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;
import com.virtualprogrammers.employeemanagement.dataaccess.EmployeeDataAccess;
@Stateless
public class EmployeeManagementImplementation implements EmployeeManagementService,EmployeeManagementServiceLocal  {
	@Inject
	private EmployeeDataAccess dao;
	
	@Override
	public List<Employee> getAllEmployees() {
		return dao.findAll();
	}

	@Override
	public List<Employee> searchBySurname(String surname) {
		return dao.findBySurname(surname);
	}

	@Override
	public Employee searchById(int id) {
		return dao.findById(id);
	}
	
	
	

}
