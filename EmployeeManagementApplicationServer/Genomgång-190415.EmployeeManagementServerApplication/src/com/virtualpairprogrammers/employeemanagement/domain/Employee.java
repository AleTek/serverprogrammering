package com.virtualpairprogrammers.employeemanagement.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
//JPA-Annotationer
//Entity f�r JPA, H�r anv�nder vi Hibernate
@Entity
@XmlRootElement
public class Employee implements Serializable{

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
@Column(length=25)
private String firstName;
@Column(length=25)
private String surname;
@Column(length=25)
private String jobRole;
private int salary;

public Employee() {
}

public Employee(String firstName, String surname, String jobrole, int salary) {
	super();
	this.firstName = firstName;
	this.surname = surname;
	this.jobRole = jobrole;
	this.salary = salary;
}


public int getId() {
	return id;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getSurname() {
	return surname;
}

public void setSurname(String surname) {
	this.surname = surname;
}

public String getJobRole() {
	return jobRole;
}

public void setJobRole(String jobRole) {
	this.jobRole = jobRole;
}

public int getSalary() {
	return salary;
}

public void setSalary(int salary) {
	this.salary = salary;
}

@Override
public String toString() {
	return this.firstName + " "+this.surname + " " + this.jobRole;
}

}
