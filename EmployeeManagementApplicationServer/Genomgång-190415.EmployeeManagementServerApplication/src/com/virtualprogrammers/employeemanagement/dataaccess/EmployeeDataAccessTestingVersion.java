package com.virtualprogrammers.employeemanagement.dataaccess;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;
@Stateless
@Alternative
public class EmployeeDataAccessTestingVersion implements EmployeeDataAccess {

	@Override
	public List<Employee> findAll() {
		Employee emp1 = new Employee("Musse", "Pigg","Deckare", 1700);
		Employee emp2 = new Employee("Kalle", "Anka","Mångsysslare", 2100);
		Employee emp3 = new Employee("Kajsa", "Anka","Kanonfotograf", 2100);
		List<Employee> employees = new ArrayList<>();
		employees.add(emp1);
		employees.add(emp2);
		employees.add(emp3);
		return employees;
	}

	@Override
	public List<Employee> findBySurname(String surname) {
		return null;
	}

	@Override
	public Employee findById(int id) {
		return null;
	}

}
