package com.virtualprogrammers.employeemanagement.dataaccess;

import java.util.List;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

public interface EmployeeDataAccess {
 
	public List<Employee>findAll();
	public List<Employee>findBySurname(String surname);
	public Employee findById(int id);
}
