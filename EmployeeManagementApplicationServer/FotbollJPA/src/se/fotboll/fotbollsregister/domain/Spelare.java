package se.fotboll.fotbollsregister.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Spelare implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(length=25)
	private String f�rnamn;
	@Column(length=25)
	private String efternamn;
	private int f�delse�r;
	@Column(length=25)
	private String lag;
	
	public Spelare() {}
	
	public Spelare(String f�rnamn, String efternamn, int f�delse�r, String lag) {
		this.f�rnamn = f�rnamn;
		this.efternamn = efternamn;
		this.f�delse�r = f�delse�r;
		this.lag = lag;
	}

	@Override
	public String toString() {
		return f�rnamn + efternamn + ", " + f�delse�r + ", " + lag;
	}
	
}
