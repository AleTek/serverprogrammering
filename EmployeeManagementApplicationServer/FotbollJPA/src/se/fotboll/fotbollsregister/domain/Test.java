package se.fotboll.fotbollsregister.domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Test {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("fotbollDb");
		
		EntityManager em = emf.createEntityManager();
		
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Spelare lallana = new Spelare("Adam", "Lallana", 1981, "Liverpool");
		em.persist(lallana);
		tx.commit();
		em.close();
	}
}
