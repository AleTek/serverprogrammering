import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import se.fotboll.fotbollsregister.FotbollsregisterService;
import se.fotboll.fotbollsregister.domain.Spelare;

public class Main {

	public static void main(String[] args) throws NamingException {
		Properties jndiProperties = new Properties();
//		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
		jndiProperties.put("jboss.naming.client.ejb.context", true);
		
		Context jndi = new InitialContext(jndiProperties);
		
		FotbollsregisterService service = (FotbollsregisterService)jndi.lookup("FotbollsregisterApplicationServer/FotbollsregisterImplementation!"
				+ "se.fotboll.fotbollsregister.FotbollsregisterService");
		
		List<Spelare>register = service.taFramSpelare();
		
		for (Spelare spelare : register) {
			System.out.println(spelare);
		}
	}

}
