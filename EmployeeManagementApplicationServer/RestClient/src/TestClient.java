import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

public class TestClient {

	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
//		WebTarget target = client.target("http://localhost:8080/EmployeeManagement/webservice/employees/101");
//		Invocation invocation = target.request().buildGet();
//		Response response = invocation.invoke();

		Response response = client.target("http://localhost:8080/EmployeeManagement/webservice/employees/101")
//				.request().buildGet().invoke();
				.request("application/JSON").buildGet().invoke();
		
		System.out.println(response.getHeaders().toString());
		System.out.println(response.getStatus());

//		Visa som str�ng, xml eller json
		System.out.println(response.readEntity(String.class));
		response.close();

//		Visa som objekt
//		Employee employee = response.readEntity(Employee.class);
//		System.out.println(employee);
//		response.close();
		
		//Create a new employee
		Employee james = new Employee();
		james.setFirstName("James");
		james.setSurname("Green");
		james.setJobRole("Author");
		james.setSalary(9000);
		
		Entity jamesEntity = Entity.entity(james, "application/JSON");
		response = client.target("http://localhost:8080/EmployeeManagement/webservice/employees")
				.request().buildPost(jamesEntity).invoke();
		System.out.println("Creating a new employee returned status code of " + response.getStatus());
		
		if(response.getStatus() == 201) {
			System.out.println(response.getHeaders().toString());
//		System.out.println(response.readEntity(Employee.class).getId());
		System.out.println(response.readEntity(String.class));
		}
		
		System.out.println(response.readEntity(Employee.class).getId());
		response.close();

//		GET ALL employees, som XML
		response = client.target("http://localhost:8080/EmployeeManagement/webservice/employees").request().buildGet()
				.invoke();
//		System.out.println(response.readEntity(String.class)); 
		List<Employee> employees = response.readEntity(new GenericType<List<Employee>>() {});
		for (Employee e : employees) {
			System.out.println(e);
		}
	}
}
