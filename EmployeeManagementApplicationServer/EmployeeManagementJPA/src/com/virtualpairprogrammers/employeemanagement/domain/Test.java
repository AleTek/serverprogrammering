package com.virtualpairprogrammers.employeemanagement.domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Test {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("employeeDb");
		
		EntityManager em = emf.createEntityManager();
		
		EntityTransaction tx= em.getTransaction();
		tx.begin();
		Employee employee1 = new Employee("Kalle", "Kula", "Utkik", 1250);
		em.persist(employee1);
		tx.commit();
		em.close();
	}

}
