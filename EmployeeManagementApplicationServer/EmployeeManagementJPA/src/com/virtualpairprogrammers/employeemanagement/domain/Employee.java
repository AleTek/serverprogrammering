package com.virtualpairprogrammers.employeemanagement.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(length=25)
	private String firstName;
	@Column(length=25)
	private String surname;
	@Column(length=25)
	private String jobRole;
	private int salary;

	public Employee() {
		//Kr�vs av JPA
	}
	
	public Employee(String firstName, String surname, String jobRole, int salary) {
		super();
		this.firstName = firstName;
		this.surname = surname;
		this.jobRole = jobRole;
		this.salary = salary;
	}
	
	public String toString() {
		return "Employee: " + this.firstName + " " + this.surname;
	}
	
}
